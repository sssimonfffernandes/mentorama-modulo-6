const resolverProblemas = (f) => {
    return (x=0, y=0) => {
      return f(x, y);
    };
   };
   ///Exercícios Módulo 2///
   //multiplicação
   const multiplicar = (num) => {
     return num * 2;
   };
   
   //adição
   const dividir =(num) => {
     return num / 2;
   };
   
   //compor nome e sobrenome
   const nomeSobrenome = (nome, sobrenome) => {
     return console.log(`${nome} ${sobrenome}`);
   };
   
   //coparar números
   const comparar = (numA, numB) => {
     if (numA > numB){
           return 'Maior';
       } if (numA < numB){
           return 'Menor';
       } if (numA === numB){
           return 'Igual';
       }
   };
   
   //corverter para fahrenheit
   const conversor = (celsius) => {
     fahrenheit = celsius * 1.8 + 32;
       return fahrenheit;
   };
   //instanciar as funções em variáveis módulo 2
   const m = resolverProblemas(multiplicar);
   const d = resolverProblemas(dividir);
   const nS = resolverProblemas(nomeSobrenome);
   const c1 = resolverProblemas(comparar);
   const c2 = resolverProblemas(conversor);
   
   //printar
   console.log(m(9));
   console.log(d(6));
   console.log(nS('Simon', 'Fernandes'));
   console.log(c1(19, 56));
   console.log(c2(23));
   
   ///Exercícios Módulo 3///
   const  checkOut  = (valor, parcelas) => {
     let parcelasMinimas = 12;
     let valorParcela;
     let resultado = '';
     if (parcelas < parcelasMinimas){
       console.log('Aquantidade mínima de parcelas é 12.');
     } else if(parcelas >= 12 && parcelas < 24){
       valorParcela = valor / parcelas;
     }
     else if (parcelas >= 24 && parcelas < 36){
       valorParcela = valor / parcelas;
       valorParcela * 0.1;
       console.log(`Você pagará ${parcelas} parcelas de R$ ${valorParcela}.`);
     } else if (parcelas >= 36){
       valorParcela = valor / parcelas;
       valorParcela * 0.15;
       console.log(`Você pagará ${parcelas} parcelas de R$ ${valorParcela}.`);
     } else {
       console.log('Valor inválido');
     }
     return `R$ ${valorParcela}` ;
   }
   
   //instanciar exercício módulo 3
   const valor = resolverProblemas(checkOut);
   
   //printar
   console.log(valor(200, 50));
   
   ///Exercícios Módulo 4
   const fibonacci = (n) => {
     let fibonacciNum = [];
     fibonacciNum[0] = 0;
     fibonacciNum[1] = 1
    for(let i=2; i <=n; i++){
      fibonacciNum[i] = fibonacciNum[i - 2] + fibonacciNum[i - 1];
    };
    return fibonacciNum;
   };
    
    //instanciar exercício módulo 4
    const fibbo = resolverProblemas(fibonacci);
   
    //printar
    console.log(fibbo(10));
   
   ///Exercícios Módulo 5//
   
   const funcSoma = (f) => {
     let soma = f.reduce((acc, cur) => acc + cur, 0);
     console.log(soma);
   
   };
   
   const funcPar = (f) => {
     // let dobro = f.arrDrobro;
     const par = f.filter((elemento) => elemento % 2 === 0);
     console.log(par);
   };
   
   const funcArr = (arr) => {
       // Multiplicar cada elemento por 2
       let arrDrobro = arr.map((elemento) => elemento * 2);
       return arrDrobro; 
   };
   //array
   let numSeq = [10, 13, 25, 8, 73, 40, 3];
   
   //instaciar exercício módulo 5 dobro
   const exercicioArr = resolverProblemas(funcArr);
   
   //printar
   console.log(exercicioArr(numSeq));
   funcPar(funcArr(numSeq));
   funcSoma(funcArr(numSeq));